-module(sleepsort).
-export([sleepsort/2, sleep/2]).

% Just for fun

% 30> c(sleepsort).
% {ok,sleepsort}
% 31> flush().
% ok
% 32> sleepsort:sleepsort([2,31,6,87,28,23,3], self()).
% ok
% 33> flush().
% Shell got 2
% Shell got 3
% Shell got 6
% Shell got 23
% Shell got 28
% Shell got 31
% Shell got 87
% ok

sleepsort([N|Rest], Pid) ->
  spawn(?MODULE, sleep, [N, Pid]),
  sleepsort(Rest, Pid);
sleepsort([], _Pid) ->
  ok.


sleep(N, Pid) ->
  receive
  after N*10  % should increase the chance of a correct sort
    -> Pid ! N
  end.