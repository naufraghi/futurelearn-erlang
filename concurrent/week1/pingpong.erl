-module(pingpong).
-export([ping/0, pong/0]).

ping() ->
  io:format("Starting ping~n"),
  receive
    stop ->
      io:format("stop ping~n");
    PongPid ->
      io:format("ping~n"),
      timer:sleep(500),
      PongPid ! self(),
      ping()
  end.

pong() ->
  io:format("Starting pong~n"),
  receive
    stop ->
      io:format("stop pong~n");
    PingPid ->
      io:format("pong~n"),
      timer:sleep(500),
      PingPid ! self(),
      pong()
  end.