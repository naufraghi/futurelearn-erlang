-module(palindrome).
-export([start/0, server/1, server/0, multiserver/1, echo/0]).

% 1> c(palindrome).
% 2> palindrome:start().

start() ->
  Echo = spawn(?MODULE, echo, []),
  io:format("server/1~n"),
  Server1 = spawn(?MODULE, server, [Echo]),
  Server1 ! {check, "Abba"},
  Server1 ! stop,
  timer:sleep(100),
  io:format("server/0~n"),
  Server2 = spawn(?MODULE, server, []),
  Server2 ! {check, Echo, "Abba"},
  Server2 ! {stop, Echo},
  timer:sleep(100),
  io:format("multiserver/1~n"),
  Multi = multiserver(3),
  Multi ! {check, Echo, "Abba"},
  Multi ! {check, Echo, "babba"},
  Multi ! {check, Echo, "Acca"},
  Multi ! {check, Echo, "Madam I'm Adam"},
  Multi ! {stop, Echo}.



echo() ->
  receive
    Message -> io:format("echo: ~p~n", [Message])
  end,
  echo().

% Palindrome check

rem_punct(String) -> lists:filter(fun (Ch) ->
                                      not(lists:member(Ch,"\"\'\t\n "))
                                    end,
                                    String).

to_small(String) -> lists:map(fun(Ch) ->
                                  case ($A =< Ch andalso Ch =< $Z) of
                                      true -> Ch+32;
                                      false -> Ch
                                   end
                                 end,
                                 String).

palindrome_check(String) ->
    Normalise = to_small(rem_punct(String)),
    lists:reverse(Normalise) == Normalise.

% Server

server(Pid) ->
  receive
    {check, Message} ->
      Res = case palindrome_check(Message) of
        true -> Message ++ " is a palindrome!";
        false -> "Nope " ++ Message
      end,
      Pid ! {result, Res},
      server(Pid);
    stop ->
      Pid ! {stop, "stopping"}
  end.

server() ->
  receive
    {check, Pid, Message} ->
      io:format("response from server ~w~n", [self()]),
      Res = case palindrome_check(Message) of
        true -> Message ++ " is a palindrome!";
        false -> "Nope " ++ Message
      end,
      Pid ! {result, Res},
      server();
    {stop, Pid} ->
      Pid ! {stop, "stopping"}
  end.

multiserver([Server|Rest]) ->
  receive
    {check, Pid, Message} ->
      Server ! {check, Pid, Message},
      multiserver(Rest ++ [Server]);
    {stop, Pid} ->
      lists:map(fun (Ser) -> Ser ! {stop, Pid} end, [Server|Rest])
  end;
multiserver(Num) when erlang:is_number(Num) ->
  Servers = lists:map(fun (_) -> spawn(?MODULE, server, []) end, lists:seq(1, Num)),
  spawn(?MODULE, multiserver, [Servers]).