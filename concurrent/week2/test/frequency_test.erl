-module(frequency_test).
-include_lib("eunit/include/eunit.hrl").
-include_lib("stdlib/include/assert.hrl").

%% Tests for the raw messages

start_test() ->
  {ok, _} = frequency:start(),
  ?assertNotEqual(undefined, erlang:whereis(frequency),
                  "should be running"),
  % TODO: Is this a flaky test? Is start is guaranteed
  % to return only after the registration is complete?
  ok.

start_twice_test() ->
  {ok, _} = frequency:start(),
  {ok, already_registered} = frequency:start(),
  ok.

receive_timeout() ->
  receive Msg -> Msg after 200 -> fail end.

star_stop_test() ->
  {ok, _} = frequency:start(),
  frequency ! {request, self(), stop},
  {reply, stopped} = receive_timeout(),
  ok.

single_allocation_per_pid_test() ->
  {ok, _} = frequency:start(),
  % Allocate once
  frequency ! {request, self(), allocate},
  {reply, {ok, Freq}} = receive_timeout(),
  % Allocate twice
  frequency ! {request, self(), allocate},
  {reply, {error, already_allocated}} = receive_timeout(),
  % Deallocate
  frequency ! {request, self(), {deallocate, Freq}},
  {reply, {ok, deallocated}} = receive_timeout(),
  ok.

deallocate_own_freqs_test() ->
  {ok, _} = frequency:start(),
  % Allocate once
  frequency ! {request, self(), allocate},
  {reply, {ok, Freq}} = receive_timeout(),
  % Wrong deallocate
  Self = self(),
  Proxy = spawn(fun () -> Self ! receive_timeout() end),
  frequency ! {request, Proxy, {deallocate, Freq}},
  {reply, {error, freq_pid_mismatch}} = receive_timeout(),
  % Real deallocate
  frequency ! {request, self(), {deallocate, Freq}},
  {reply, {ok, deallocated}} = receive_timeout(),
  ok.

%% Tests for the functional interface

allocate_deallocate_test() ->
  {ok, _} = frequency:start(),
  % NOTE: `self()` inside `allocate/0` is the caller `self()`
  {ok, Freq} = frequency:allocate(),
  % Deallocate
  {ok, deallocated} = frequency:deallocate(Freq),
  ok.

allocate_deallocate_owner_test() ->
  {ok, _} = frequency:start(),
  % NOTE: `self()` inside `allocate/0` is the caller `self()`
  {ok, Freq} = frequency:allocate(),
  % Wrong deallocate
  Self = self(),
  spawn(fun () ->
          Self ! frequency:deallocate(Freq)  % `self()` here is the child Pid
        end),
  {error, freq_pid_mismatch} = receive_timeout(),
  % Real deallocate
  {ok, deallocated} = frequency:deallocate(Freq),
  ok.

% Helpers for concurrent test

client(Parent) ->
  spawn(fun () -> test_client(Parent, unset) end).

test_client(Parent, Freq) ->
  %?debugVal({test_client, Parent, Freq}),
  receive
    {proxy, allocate} ->
        case ?debugVal(frequency:allocate()) of
          {ok, NewFreq} ->
            test_client(Parent, NewFreq);
          _ ->
            test_client(Parent, unset)
        end;
    {proxy, deallocate} ->
        ?debugVal({deallocate, Freq}),
        case Freq of
          unset ->
            test_client(Parent, unset);
          _ ->
            test_client(Parent, unset)
        end;
    {proxy, stop} ->
        frequency:stop();
    Msg ->
        ?debugVal(Msg),
        test_client(Parent, Freq)
   end.

allocate_and_stop_test() ->
  timer:sleep(100), % consume old sleeps
  frequency:start(),
  timer:sleep(10), % let it start
  lists:foreach(fun (I) ->
      Client = client(self()),
      case I of
        1 -> Client ! ?debugVal({proxy, stop});
        _ -> Client ! ?debugVal({proxy, allocate})
      end,
      Client
    end, [1,2,3,4,5]).