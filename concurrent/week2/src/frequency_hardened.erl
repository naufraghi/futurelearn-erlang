%% Based on code from 
%%   Erlang Programming
%%   Francecso Cesarini and Simon Thompson
%%   O'Reilly, 2008
%%   http://oreilly.com/catalog/9780596518189/
%%   http://www.erlangprogramming.org/
%%   (c) Francesco Cesarini and Simon Thompson

-module(frequency_hardened).
-export([start/0,allocate/0,deallocate/1,stop/0]).
-export([init/0]).

%% These are the start functions used to create and
%% initialize the server.

start() ->
    register(frequency,
	     spawn(?MODULE, init, [])).

init() ->
  process_flag(trap_exit, true),    %%% ADDED
  Frequencies = {get_frequencies(), []},
  loop(Frequencies).

% Hard Coded
get_frequencies() -> [10,11,12,13,14,15].

%% The Main Loop

loop(Frequencies) ->
  receive
    {request, Pid, allocate} ->
      {NewFrequencies, Reply} = allocate(Frequencies, Pid),
      Pid ! {reply, Reply},
      loop(NewFrequencies);
    {request, Pid , {deallocate, Freq}} ->
      NewFrequencies = deallocate(Frequencies, Freq),
      Pid ! {reply, ok},
      loop(NewFrequencies);
    {request, Pid, stop} ->
      Pid ! {reply, stopped};
    {'EXIT', Pid, _Reason} ->                   %%% CLAUSE ADDED
      NewFrequencies = exited(Frequencies, Pid), 
      loop(NewFrequencies)
  end.

%% Functional interface

allocate() -> 
    frequency ! {request, self(), allocate},
    receive 
      {reply, Reply} -> Reply;
      _              -> ok
    end.

deallocate(Freq) -> 
    frequency ! {request, self(), {deallocate, Freq}},
    receive 
      {reply, Reply} -> Reply;
      _              -> ok
    end.

stop() -> 
    frequency ! {request, self(), stop},
    receive 
	    {reply, Reply} -> Reply
    end.


%% The Internal Help Functions used to allocate and
%% deallocate frequencies.
%%
%% There is no risk of race conditions in these bocks, because the caller
%% in this flow is the frequency process, and a process can work only on a
%% single task:
%% 1. frequency:allocate(Pid) is a caller controlled flow,
%% 2. the function communicates with a message with the frequency process,
%% 3. now the execution is linear, inside the process.

-type allocated() :: [{integer(), pid()}].
-type free_freqs() :: [integer()].
-type freqs() :: {free_freqs(), allocated()}.

-spec allocate(freqs(), pid()) -> {freqs(), {ok, integer()} | {error, no_frequency}}.
allocate({[], Allocated}, _Pid) ->
  {{[], Allocated}, {error, no_frequency}};
allocate({[Freq|Free], Allocated}, Pid) ->
  link(Pid),                                               %%% ADDED
  {{Free, [{Freq, Pid}|Allocated]}, {ok, Freq}}.

-spec deallocate(freqs(), integer()) -> freqs().
deallocate({Free, Allocated}, Freq) ->
  {value,{Freq,Pid}} = lists:keysearch(Freq,1,Allocated),  %%% ADDED
  NewAllocated=lists:keydelete(Freq, 1, Allocated),
  case lists:keysearch(Pid,2,Allocated) of
    false ->
      unlink(Pid);                                         %%% ADDED
    _ -> ok
  end,
  {[Freq|Free], NewAllocated}.

-spec exited(freqs(), pid()) -> freqs().
exited({Free, Allocated}, Pid) ->                %%% FUNCTION ADDED
    case lists:keysearch(Pid,2,Allocated) of
      {value,{Freq,Pid}} ->
        NewAllocated = lists:delete({Freq,Pid}, Allocated),
        exited({[Freq|Free],NewAllocated}, Pid); 
      false ->
        {Free,Allocated} 
    end.
