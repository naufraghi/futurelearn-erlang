-module(echo).
-export([listener/0]).


listener() ->
    receive
        {Pid,M} ->
            io:format("~w received from ~w.~n",[M,Pid]),
            timer:sleep(500),
            Pid!M,
            io:format("~w echoed.~n",[M]),
            listener()
    end.
