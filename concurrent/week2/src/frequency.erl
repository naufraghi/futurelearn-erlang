%% Based on code from
%%   Erlang Programming
%%   Francecso Cesarini and Simon Thompson
%%   O'Reilly, 2008
%%   http://oreilly.com/catalog/9780596518189/
%%   http://www.erlangprogramming.org/
%%   (c) Francesco Cesarini and Simon Thompson
-include_lib("eunit/include/eunit.hrl").

-module(frequency).
-export([start/0, stop/0, allocate/0, deallocate/1]).

%% Public functions

start() ->
  % We could also check the registration once before spawning the child
  % but we need the same to check for race conditions.
  % TODO: Is correct to assume that `register/2` can fail only if the name
  % is taken?
  Freq = spawn(fun init/0),
  try register(?MODULE, Freq) of
    _ -> {ok, registered}
  catch
    error:badarg ->
      % Stop the extra frequency actor we where unable to register.
      Freq ! {service, quit},
      ?MODULE ! {service, ack},  % FIXME: fails if the actor is stopped
      {ok, already_registered}
  end.

allocate() ->
  ?MODULE ! {request, self(), allocate},
  receive_timeout().

deallocate(Freq) ->
  ?MODULE ! {request, self(), {deallocate, Freq}},
  receive_timeout().

stop() ->
  ?MODULE ! {request, self(), stop},
  % stop is expected to clear the mailbox
  case receive_timeout() of
    {ok, Msg} ->
      unregister(?MODULE), % may fail in a rece condition
      {ok, Msg};
    Err -> Err
  end.

%% Helper functions

receive_timeout() ->
  receive
    {reply, Reply} -> Reply
  after 1000 -> {error, timeout}
  end.

%% These are the start functions used to create and
%% initialize the server.

init() ->
  Frequencies = {get_frequencies(), []},
  loop(Frequencies).

%% Hard Coded
get_frequencies() -> [10,11,12,13,14,15].

%% The Main Loop

loop(Frequencies) ->
  receive
    {request, Pid, stop} ->
      ?debugVal({request, Pid, stop, timer:sleep(1)}),
      clear(Pid);
    {request, Pid, allocate} ->
      {NewFrequencies, Reply} = allocate(Frequencies, Pid),
      Pid ! {reply, Reply},
      loop(NewFrequencies);
    {request, Pid , {deallocate, Freq}} ->
      {NewFrequencies, Reply} = deallocate(Frequencies, {Freq, Pid}),
      Pid ! {reply, Reply},
      loop(NewFrequencies);
    {service, quit} ->
      quit;
    {service, ack} ->
      loop(Frequencies)
  end.

clear(Pid) ->
  %% Empty the message queue, signaling the drop
  receive
    {request, RPid, Msg} ->
      RPid ! {reply, {dropped, Msg}},
      clear(Pid);
    _ ->
      clear(Pid)
  after 0 -> Pid ! {reply, stopped}
  end.

%% The Internal Help Functions used to allocate and
%% deallocate frequencies.
allocate({[], Allocated}, _Pid) ->
  {{[], Allocated}, {error, no_frequency}};
allocate({[Freq|Free], Allocated}, Pid) ->
  % Allocated is [{Freq, Pid},...]
  case lists:keymember(Pid, 2, Allocated) of
    true ->
      {{[Freq|Free], Allocated}, {error, already_allocated}};
    _ ->
      {{Free, [{Freq, Pid}|Allocated]}, {ok, Freq}}
  end.

deallocate({Free, Allocated}, {Freq, Pid}) ->
  % Allocated is [{Freq, Pid},...]
   case lists:keymember(Freq, 1, Allocated) of
    true ->
      % There is a frequency, check if it is associated with this Pid
      case lists:member({Freq, Pid}, Allocated) of
        true ->
          % The requested frequency matches the Pid
          NewAllocated = lists:delete({Freq, Pid}, Allocated),
          % NOTE: this check/do sequency is not at risk of race condition
          % because the only process that can execute this code is
          % the `frequency` one.
          {{[Freq|Free], NewAllocated}, {ok, deallocated}};
        false ->
          % The requested frequency does not match the Pid
          {{Free, Allocated}, {error, freq_pid_mismatch}}
      end;
    _ ->
      {{Free, Allocated}, {ok, not_allocated}}

  end.