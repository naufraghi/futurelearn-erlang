-module(super).
-export([super/0]).

super() ->
    process_flag(trap_exit, true),
    E = spawn_link(echo,listener,[]),
    register(echo,E),
    io:format("echo spawned.~n"),
    T = spawn_link(talk,worker,[]),
    register(talk,T),
    io:format("worked spawned as Pid ~w.~n",[whereis(talk)]),
    loop(E,T).

loop(E,T) ->
     receive
        {'EXIT', T, _} ->
            NewT = spawn_link(talk,worker,[]),
            register(talk,NewT),
            io:format("worked re-spawned as Pid ~w.~n",[whereis(talk)]),
            loop(E,NewT);
         {'EXIT', E, _} ->
            % The echo process is restarted, but we may have the previous
            % instance killed after receiving the message but before
            % sending back the reply. We can add an `after` in `work/1`
            % and retry the same N again.
            timer:sleep(1000),
            NewE = spawn_link(echo,listener,[]),
            register(echo,NewE),
            io:format("echo re-spawned.~n"),
            loop(NewE,T)
    end.

% This is tha trace of an echo kill when talk is sending the message.
% The sending fails and the worker is restarted.
%
% 11 received from <0.238.0>.
% 11 echoed.
% 45> exit(whereis(echo), kill).
% true
% 46> =ERROR REPORT==== 14-Jul-2020::19:19:10.191058 ===
% Error in process <0.238.0> with exit value:
% {badarg,[{talk,work,1,[{file,"talk.erl"},{line,10}]}]}
%
% echo re-spawned.
% worked re-spawned as Pid <0.242.0>.

% This is the trace of an echo kill between the send and the receive.
% The worker `after` triggers a retry.
%
% 46> exit(whereis(echo), kill).
% true
% {<0.242.0>,2} not echoed, retry
% echo re-spawned.

% Sometime a `c(talk).` can trigger a:
%
% 47> c(talk).
% {ok,talk}
%
% But other times it can:
%
% 48> c(talk).
% worked re-spawned as Pid <0.254.0>.
% {ok,talk}
%
% Why?