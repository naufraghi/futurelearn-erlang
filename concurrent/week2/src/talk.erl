-module(talk).
-export([worker/0]).

worker() ->
    work(0).

work(N) ->
    timer:sleep(500),
    Msg = {self(),N},
    echo!Msg,
    io:format("~w sent.~n",[Msg]),
    receive
        _Reply ->
            work(N+1)
    after 1000 ->
        io:format("~w not echoed, retry~n",[Msg]),
        work(N)
    end.

