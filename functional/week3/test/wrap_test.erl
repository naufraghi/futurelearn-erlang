-module(wrap_test).
-include_lib("eunit/include/eunit.hrl").

normalize_test() ->
    "Normalized spaces" = wrap:normalize("Normalized    spaces   \n"),
    ok.

wrap_text_test() ->
    ["Lorem ipsum dolor sit amet,",
     "consectetur adipiscing elit, sed",
     "do eiusmod tempor incididunt ut",
     "labore et dolore magna aliqua."] = wrap:wrap_text("Lorem ipsum dolor sit amet,\n consectetur adipiscing
                                                         elit, sed\n do eiusmod tempor incididunt ut labore et
                                                         dolore          magna aliqua.", 32),
    ok.

repeat_test() ->
    [" ", " ", " "] = wrap:repeat(" ", 3),
    [] = wrap:repeat(" ", 0),
    ok.

interleave_test() ->
    [1,2,1,2,1] = wrap:interleave([1,1,1], [2,2]),
    ok.

justify_text_test() ->
    ["1 3 5 7 9"] = wrap:justify_text("1  3  5  7  9", 9),
    ["Lorem   ipsum  dolor  sit  amet,",
     "consectetur adipiscing elit, sed",
     "do  eiusmod tempor incididunt ut",
     "labore  et  dolore magna aliqua."] = wrap:justify_text("Lorem ipsum dolor sit amet,\n consectetur adipiscing
                                                               elit, sed\n do eiusmod tempor incididunt ut labore et
                                                               dolore          magna aliqua.", 32),
    ok.
