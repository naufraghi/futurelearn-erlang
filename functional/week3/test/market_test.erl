-module(market_test).
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

lookup_test() ->
    {"Dry Sherry, 1lt", 540} = market:lookup(1234),
    ok.

make_bill_test() ->
    [{"Hula Hoops", 21}
    ,{"Hula Hoops (Giant)", 133}] = market:make_bill([1111, 1112]),
    ok.

total_test() ->
    154 = market:total([{"Hula Hoops", 21}
                       ,{"Hula Hoops (Giant)", 133}]),
    ok.

add_total_test() ->
    [{"Hula Hoops", 21}
    ,{"Hula Hoops (Giant)", 133}
    ,{total, 154}] = market:add_total([{"Hula Hoops", 21}
                                      ,{"Hula Hoops (Giant)", 133}]),
    ok.

right_pad_test() ->
    ".......Foo Bar" = market:right_pad(14, "Foo Bar"),
    "Foo Bar" = market:right_pad(5, "Foo Bar"),
    ok.

format_line_test() ->
    % I'm unable to test in a meaningful way if I put an € sign as currency
    "Hula Hoops..................£0.21\n" = market:format_line({"Hula Hoops", 21}),
    ok.

show_test() ->
    "      Erlang Market
Hula Hoops..................£0.21
Hula Hoops (Giant)..........£1.33

Total.......................£1.54
" = market:show([1111, 1112]),
    ok.

-endif.