-module(index_test).
-include_lib("eunit/include/eunit.hrl").

enumerate_test() ->
    [{1, "line1"}] = index:enumerate(["line1"]),
    [{2, "line2"},{1, "line1"}] = index:enumerate(["line1", "line2"]),
    ok.

merge_ranges_test() ->
    [{10,10},{8,8},{5,6},{1,3}] = index:merge_ranges([1,2,3,5,6,8,10]),
    ok.

reversed_index_test() ->
    [{"a",2},
     {"happy",2},
     {"story",2},
     {",",2},
     {"this",1},
     {"is",1},
     {"this",2}] =
    index:reversed_index(index:enumerate(["this is", "a happy story , this"])),
    ok.

merge_reversed_index_test() ->
    ReverseIndex = [{"a",2},
	            {"happy",2},
	            {"story",2},
	            {"this",1},
	            {"is",1},
	            {"this",2}],
    [{"happy",[{2,2}]},
     {"story",[{2,2}]},
     {"this",[{1,2}]}] = index:merge_reversed_index(ReverseIndex),
    ok.
