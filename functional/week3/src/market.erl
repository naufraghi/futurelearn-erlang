-module(market).
-export([my_cart/0, price_list/0, lookup/1, make_bill/1, total/1, add_total/1, format_line/1, right_pad/2]).
-export([show/1]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Types
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Shopping list Item
-type item() :: string().
% Cost
-type cost() :: integer().
% Barcode value
-type code() :: integer().

% A shopping cart results in a list of codes
-type codes() :: list(code()).

% The database links an item to a code and a price
-type database() :: list({code(),item(),cost()}).

% The list of the bought items
-type bill() :: list({item(),cost()}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Example data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-spec my_cart() -> codes().
my_cart() -> [1234,4719,3814,1112,1113,1234].

-spec price_list() -> database().
price_list() -> [{4719, "Fish Fingers" , 121},
                 {5643, "Nappies" , 1010},
                 {3814, "Orange Jelly", 56},
                 {1111, "Hula Hoops", 21},
                 {1112, "Hula Hoops (Giant)", 133},
                 {1234, "Dry Sherry, 1lt", 540}].

-spec lookup(code(),database()) -> {item(),cost()}.
lookup(Code, DB) ->
    lookup_sorted(Code, lists:sort(DB)).

% Lookup will fail if the code is not present
-spec lookup_sorted(code(),database()) -> {item(),cost()}.
lookup_sorted(Code, [{Code,Item,Price}|_DB]) ->
    {Item,Price};
lookup_sorted(Code, [{ICode,_,_}|DB]) when Code > ICode ->
    lookup_sorted(Code, DB).

% Lkup on the example data
-spec lookup(code()) -> {item(),cost()}.
lookup(Code) -> lookup(Code, price_list()).

% Bill on the example data
-spec make_bill(codes()) -> bill().
make_bill([]) -> [];
make_bill([C|Cs]) ->
    [lookup(C)|make_bill(Cs)].

% Compute the total from a bill
-spec total(bill()) -> cost().
total([]) ->
    0;
total([{_,C}|Cs]) ->
    C + total(Cs).

% Generate a bill with the total
-spec add_total(bill()) -> bill().
add_total(Bill) ->
    Bill ++ [{total, total(Bill)}].

% To present the lines, we need a way to format an {item(),cost()}.
-spec format_line({item(),cost()}) -> string().
format_line({Item, Cost}) ->
    % Put the description, write the cost, padding with the right number of '.'
    lists:flatten(io_lib:format("~s~*...ts~.2f~n", [Item, 30 - 1 - length(Item), "£", Cost / 100])).

% Unused right_pad
-spec right_pad(integer(), string()) -> string().
right_pad(Len, String) when Len > 0 ->
    rpad(Len - length(String), String).

rpad(Len, String) when Len > 0 ->
    [hd(".")|rpad(Len-1, String)];
rpad(_, String) ->
    String.

% Format bill
-spec show(bill()|{item(),cost()|codes()}) -> string().
show({total, Cost}) ->
    [hd("\n")|format_line({"Total", Cost})];
show({Item, Cost}) ->
    format_line({Item, Cost});
show([{Item, Cost}]) ->
    show({Item, Cost});
show([{Item, Cost}|Bill]) ->
    show({Item, Cost}) ++ show(Bill);
show(Codes) ->
    "      Erlang Market\n" ++ show(add_total(make_bill(Codes))).
