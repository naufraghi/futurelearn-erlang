-module(wrap).
-export([normalize/1, wrap_text/2, justify_text/2, repeat/2, interleave/2]).

% Given some text we first remove all extra spacing

normalize(Text) ->
    % Remove extra spacing
    normalize(Text, "\n\r\t ").

normalize(String, []) ->
    String;
normalize(String, [I|Is]) ->
    normalize(
        string:join(
            lists:filter(
                fun(S) -> length(S) > 0 end,
                string:replace(String, [I], "", all)
            ), " "),
        Is).

% The public wrap_text/2 functions delegates the work the a raw one.

wrap_text(Text, Width) when Width > 0 ->
    Wrapped = wrap_text_raw(Text, Width),
    raw_to_text(Wrapped).

% Helper function to produce back the plain lines from the
% {ListOfWords, NumOfLetters} tuple.

raw_to_text(Wrapped) ->
    raw_to_text(Wrapped, " ").
raw_to_text(Wrapped, Space) ->
    lists:map(fun({W,_C}) -> lists:flatten(lists:join(Space, W)) end, Wrapped).

% We start creating the new lines, given some width,
% we concatenate the next word to the current line if the
% width fits. We start a new line if not.

wrap_text_raw(Text, Width) when Width > 0 ->
    NormText = normalize(Text),
    Words = string:lexemes(NormText, " "),
    wrap_text_raw(Words, Width, [{[],0}]).

% After normalize and split, we start the real recursion.
% The lines are stored in a tuple:
%  - the first element is the list of words,
%  - the second element is the count of the letters of all the words in the list.
% With this structure the "real" length is computed using a single space between words.

wrap_text_raw([], _, [{Line,Len}|Wrapped]) ->
    lists:reverse([{Line,Len}|Wrapped]);
wrap_text_raw([Word|RevWords], Width, [{Line,Len}|Wrapped]) when (Len + length(Line) + length(Word)) =< Width ->
    % I can accumulate the processed lines in reverse, but not the words, because
    % we want to fill the lines from the top and not from the bottom of the paragraph:
    %   we can accept
    %   a last half-empty paragraph, but not a first one.
    % The `Len` considers the non blank spaces only
    wrap_text_raw(RevWords, Width, [{Line ++ [Word], Len + length(Word)}|Wrapped]);
wrap_text_raw([Word|RevWords], Width, Wrapped) ->
    wrap_text_raw(RevWords, Width, [{[Word],length(Word)}|Wrapped]).

% Justify the text
justify_text(Text, Width) when Width > 0 ->
    Lines = wrap_text_raw(Text, Width),
    Justified = justify_text(Lines, Width, []),
    raw_to_text(Justified, "").


justify_text([], _Width, Justified) ->
    lists:reverse(Justified);
justify_text([{Line, Num}|Lines], Width, Justified) ->
    % Number of spaces between words.
    NumSingleSpaces = length(Line) - 1,
    % Number of missing spaces to reach the width
    NumSpacesToAdd = Width - Num,
    % We can add some "space" words between the real words to reach the line length.
    NumSpacesForAll = NumSpacesToAdd div NumSingleSpaces,
    % Should be one or more space for all, and (maybe) one space between some words.
    NumSpaceForSome = NumSpacesToAdd rem NumSingleSpaces,
    % This is the number of spaces to distribute between every word
    SpacesForAll = lists:flatten(lists:duplicate(NumSpacesForAll, " ")),
    SpacesForAllList = repeat(SpacesForAll, NumSingleSpaces),
    % This is the number of words separated by an uneven extra space (we decide to add on the start of the line)
    SpacesForSomeList = repeat(" ", NumSpaceForSome) ++ repeat("", NumSingleSpaces - NumSpaceForSome),
    % The list of spaces is the composition of these lists
    Spaces = lists:zipwith(fun(X,Y) -> X ++ Y end, SpacesForAllList, SpacesForSomeList),
    % Interleaving N words with N-1 spaces, we reach the wanted column
    JustifiedRaw = {interleave(Line, Spaces), Num + NumSpacesToAdd},
    justify_text(Lines, Width, [JustifiedRaw|Justified]).


% Return a list with the element Element repeated Num times
repeat(Element, Num) ->
    repeat(Element, Num, []).

repeat(_Element, 0, List) ->
    List;
repeat(Element, Num, List) ->
    repeat(Element, Num - 1, [Element|List]).


% Interleave, only the special case we are using
interleave(A, B) when length(A) == length(B) + 1 ->
    interleave(A, B, []).

interleave([A|[]], [], Out) ->
    lists:reverse([A|Out]);
interleave([A|As], [B|Bs], Out) ->
    interleave(As, Bs, [B|[A|Out]]).

