-module(index).
-export([get_file_contents/1,show_file_contents/1,enumerate/1,merge_ranges/1,reversed_index/1, merge_reversed_index/1]).

% Used to read a file into a list of lines.
% Example files available in:
%   gettysburg-address.txt (short)
%   dickens-christmas.txt  (long)

% Get the contents of a text file into a list of lines.
% Each line has its trailing newline removed.

get_file_contents(Name) ->
    {ok,File} = file:open(Name,[read]),
    Rev = get_all_lines(File,[]),
    lists:reverse(Rev).

% Auxiliary function for get_file_contents.
% Not exported.

get_all_lines(File,Partial) ->
    case io:get_line(File,"") of
        eof -> file:close(File),
               Partial;
        Line -> {Strip,_} = lists:split(length(Line)-1,Line),
                get_all_lines(File,[Strip|Partial])
    end.

% Show the contents of a list of strings.
% Can be used to check the results of calling get_file_contents.

show_file_contents([L|Ls]) ->
    io:format("~s~n",[L]),
    show_file_contents(Ls);
show_file_contents([]) ->
    ok.

% Function to enumerate the lines

-spec enumerate(list(L)) -> list({pos_integer(), L}).
enumerate(Lines) ->
    enumerate(Lines, 1, []).

enumerate([], _N, Enumerate) ->
    Enumerate;
enumerate([Line|Lines], N, Enumerate) ->
    enumerate(Lines, N+1, [{N,Line}|Enumerate]).

% Create the expanded index, returning the line each word is in

reversed_index(EnumLines) ->
    reversed_index(EnumLines, []).

reversed_index([], RevIndex) ->
    RevIndex;
reversed_index([{N,Line}|EnumLines], RevIndex) ->
    RevLineIndex = lists:map(fun(Word) -> {Word,N} end, string:split(Line, " ", all)),
    reversed_index(EnumLines, lists:merge(RevLineIndex, RevIndex)).

% Now we have a list of {"word", 12} we need to merge, instead of sorting, we
% can use a map to group the indexes

merge_reversed_index(RevIndex) ->
    merge_reversed_index(RevIndex, maps:new()).

merge_reversed_index([], Index) ->
    lists:map(fun({Word, Nums}) -> {Word, merge_ranges(Nums)} end, maps:to_list(Index));
merge_reversed_index([{Word,_Pos}|RevIndex], Index) when length(Word) < 3 ->
    % skip short words
    merge_reversed_index(RevIndex, Index);
merge_reversed_index([{Word,Pos}|RevIndex], Index) ->
    AddedWordIndex = [Pos|maps:get(Word, Index, [])],
    merge_reversed_index(RevIndex, maps:put(Word, AddedWordIndex, Index)).


% `add_to_index` is a function that adds a location to an index
%
% Example index entry: { "foo" , [{3,5},{7,7},{11,13}] }
%
% The raw index will be a set, that we can convert in a range index once
% sorted.

merge_ranges(WordPositions) ->
    merge_ranges(lists:sort(WordPositions), []).

merge_ranges([], Ranges) ->
    Ranges;
merge_ranges([S|Sorted], [{Inf,Sup}|Ranges]) when (S - Sup) =< 1 ->
    % We expect the list sorted in ascending order.
    % If a new index item one page distant or less, the new item
    % can be the new Sup
    merge_ranges(Sorted, [{Inf,S}|Ranges]);
merge_ranges([S|Sorted], Ranges) ->
    % In the other cases, we have a gap, and we add a new range
    merge_ranges(Sorted, [{S,S}|Ranges]).
