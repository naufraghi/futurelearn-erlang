-module(rps).
-export([play/1,echo/1,play_two/3,rock/1,no_repeat/1,const/1,enum/1,cycle/1,rand/1,val/1,tournament/2]).
-export([least_frequent/1, most_frequent/1]).

%
% play one strategy against another, for N moves.
%

play_two(StrategyL,StrategyR,N) ->
    play_two(StrategyL,StrategyR,[],[],N).

% tail recursive loop for play_two/3
% 0 case computes the result of the tournament

% FOR YOU TO DEFINE
% REPLACE THE dummy DEFINITIONS

play_two(_,_,PlaysL,PlaysR,0) ->
   tournament(PlaysL,PlaysR);

play_two(StrategyL,StrategyR,PlaysL,PlaysR,N) ->
   PlayL = StrategyL(PlaysR),
   PlayR = StrategyR(PlaysL),
   play_two(StrategyL,StrategyR,[PlayL|PlaysL],[PlayR|PlaysR],N-1).

%
% interactively play against a strategy, provided as argument.
%

play(Strategy) ->
    io:format("Rock - paper - scissors~n"),
    io:format("Play one of rock, paper, scissors, ...~n"),
    io:format("... r, p, s, stop, followed by '.'~n"),
    play(Strategy,[],[]).

% tail recursive loop for play/1

play(Strategy,Moves,Results) ->
    {ok,P} = io:read("Play: "),
    Play = expand(P),
    case Play of
	stop ->
	    io:format("Stopped~n"),
        format_results(Results);
	_    ->
	    Result = result(Play,Strategy(Moves)),
	    io:format("Result: ~p~n",[Result]),
	    play(Strategy,[Play|Moves],[Result|Results])
    end.

%
% auxiliary functions
%

format_results(Results) ->
    {Win, Draw, Lose} = count_results(Results, {0,0,0}),
    io:format(" Wins: ~p~n Draws: ~p~n Loses: ~p~n", [Win, Draw, Lose]).

count_results([], {Win, Draw, Lose}) ->
    {Win, Draw, Lose};
count_results([R|Rs], {Win, Draw, Lose}) ->
    case R of
    win -> count_results(Rs, {Win + 1, Draw, Lose});
    draw -> count_results(Rs, {Win, Draw + 1, Lose});
    lose -> count_results(Rs, {Win, Draw, Lose + 1})
    end.


% transform shorthand atoms to expanded form

expand(r) -> rock;
expand(p) -> paper;
expand(s) -> scissors;
expand(X) -> X.

% result of one set of plays

result(rock,rock) -> draw;
result(rock,paper) -> lose;
result(rock,scissors) -> win;
result(paper,rock) -> win;
result(paper,paper) -> draw;
result(paper,scissors) -> lose;
result(scissors,rock) -> lose;
result(scissors,paper) -> win;
result(scissors,scissors) -> draw.

% result of a tournament

tournament(PlaysL,PlaysR) ->
    lists:sum(
      lists:map(fun outcome/1,
		lists:zipwith(fun result/2,PlaysL,PlaysR))).

outcome(win)  ->  1;
outcome(lose) -> -1;
outcome(draw) ->  0.

% transform 1, 2, 3 to rock, paper, scissors and vice versa.

enum(1) ->
    rock;
enum(2) ->
    paper;
enum(3) ->
    scissors.

val(rock) ->
    1;
val(paper) ->
    2;
val(scissors) ->
    3.

% give the play which the argument beats/lose.

% A beats B
beats(rock) ->
    scissors;
beats(paper) ->
    rock;
beats(scissors) ->
    paper.

% A loses against B
lose(rock) ->
    paper;
lose(paper) ->
    scissors;
lose(scissors) ->
    rock.

%
% strategies.
%
echo([]) ->
     paper;
echo([Last|_]) ->
    Last.

rock(_) ->
    rock.



% FOR YOU TO DEFINE
% REPLACE THE dummy DEFINITIONS

no_repeat([]) ->
    scissors;
no_repeat([X|_]) ->
    % Here we know that the opponent will not repeat the same game twice.
    % If `rock` was used, then the next turn will be `scissors` or `paper`.
    % Between the three:
    % - `rocks`:    `win`  against `scissors` and `lose` against `paper` =>  0
    % - `scissors`: `draw` against `scissors` and `win`  against `paper` =>  1
    % - `paper`:    `lose` against `scissors` and `draw` against `paper` => -1
    % So we'd better return the choice that would lose against the last play:
    % - `rock`     -> `scissors` vs `paper`
    % - `scissors` -> `paper` vs `rock`
    % - `paper`    -> `rock` vs `scissors`
    beats(X).

const(_Play) ->
    % Dunno what is this
    dummy.

cycle(Play) ->
    Choices = [paper, rock, scissors],
    % The only state we have is the length of the game.
    N = length(Play) rem 3,
    lists:nth(N+1, Choices).

-spec least_frequent(list(rock|paper|scissors)) -> rock|paper|scissors.
least_frequent(Play) ->
    {Rocks, Papers, Scissors} = count(Play, {0,0,0}),
    element(2, lists:min([{Rocks,    lose(rock)},
                          {Papers,   lose(paper)},
                          {Scissors, lose(scissors)}])).

-spec most_frequent(list(rock|paper|scissors)) -> rock|paper|scissors.
most_frequent(Play) ->
    {Rocks, Papers, Scissors} = count(Play, {0,0,0}),
    element(2, lists:max([{Rocks,    lose(rock)},
                          {Papers,   lose(paper)},
                          {Scissors, lose(scissors)}])).

-spec count(list(rock|paper|scissors), {integer(), integer(), integer()}) -> {integer(), integer(), integer()}.
count([], {Rocks, Papers, Scissors}) ->
    {Rocks, Papers, Scissors};
count([P|Ps], {Rocks, Papers, Scissors}) ->
    count(Ps, case P of
    rock     -> {Rocks + 1, Papers,     Scissors};
    paper    -> {Rocks,     Papers + 1, Scissors};
    scissors -> {Rocks,     Papers,     Scissors + 1}
    end).

rand(_) ->
    enum(rand:uniform(3)).
