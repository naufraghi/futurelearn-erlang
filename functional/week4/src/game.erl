-module(game).
-export([beat/1, lose/1, result/2, tournament/2]).

% rock-paper-scissors game

% What beat Rock? Paper

beat(rock) -> paper;
beat(paper) -> scissors;
beat(scissors) -> rock.

% What lose against Paper? Rock

lose(paper) -> rock;
lose(rock) -> scissors;
lose(scissors) -> paper.

% Result of a match, use integers to easy the total computation.
% We could also use win/lose/draw atoms, but 1,-1,0 are clear enough.

result(A,A) ->
    0;
result(A,B) ->
    case lose(A) of
        B -> 1;
        _ -> -1
    end.

% Tournament results

tournament(Left, Rigth) ->
    lists:sum(lists:zipwith(fun result/2, Left, Rigth)).