-module(functions).
-export([doubleAll/1, evens/1, product/1, zip/2, zip_with/3]).
-export([zip_with_map/3, zip_zip_with/2, compose/1, twice/1, iterate/1]).

% Using higher-order functions

doubleAll(L) ->
    lists:map(fun(X) -> X*2 end, L).

evens(L) ->
    lists:filter(fun(X) -> X rem 2 == 0 end, L).

product(L) ->
    lists:foldr(fun(A,B) -> A*B end, 1, L).

% Zipping

zip([X|Xs], [Y|Ys]) ->
    [{X,Y}|zip(Xs, Ys)];
zip(_, _) ->
    [].

zip_with(F, [X|Xs], [Y|Ys]) ->
    [F(X,Y)|zip_with(F, Xs, Ys)];
zip_with(_,_,_) ->
    [].

zip_with_map(F, Xs, Ys) ->
    lists:map(fun({A,B}) -> F(A,B) end, zip(Xs, Ys)).

zip_zip_with(Xs, Ys) ->
    zip_with(fun(X,Y) -> {X,Y} end, Xs, Ys).

% Composition

compose([F|Fs]) ->
    lists:foldr(fun(Fi, Acc) ->
                    fun(X) -> Fi(Acc(X)) end
                end, F, Fs).

% Twice
% What happens when you apply twice to itself?
% What happens when you apply the result of that
% to “multiply by 3” and the result of that to 2?
% -> should apply 4 times -> 162

-spec twice(fun((T) -> T)) -> fun((T) -> T).
twice(F) ->
    compose([F,F]).

% Iteration

-spec iterate(non_neg_integer()) -> fun((T) -> T).
iterate(0) ->
    fun(_F) ->
        fun(X) -> X end
    end;
iterate(N) when N > 0 ->
    fun(F) ->
        compose(lists:duplicate(N, F))
    end.