-module(functions_test).
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

% Using higher-order functions

doubleAll_test() ->
    [2,4,6] = functions:doubleAll([1,2,3]),
    ok.

evens_test() ->
    [2,4] = functions:evens([1,2,3,4]),
    ok.

product_test() ->
    24 = functions:product([1,2,3,4]),
    ok.

% Zipping

zip_test() ->
    [ {1,2}, {3,4} ] = functions:zip([1,3,5,7], [2,4]),
    [ {1,2}, {3,4} ] = functions:zip_zip_with([1,3,5,7], [2,4]),
    ok.

zip_with_test() ->
    [ 3, 7 ] = functions:zip_with(fun(X,Y) -> X+Y end, [1,3,5,7], [2,4]),
    [ 3, 7 ] = functions:zip_with_map(fun(X,Y) -> X+Y end, [1,3,5,7], [2,4]),
    ok.

% Composition

compose_test() ->
    By2 = fun(X) -> X*2 end,
    By3 = fun(X) -> X*3 end,
    6 = (functions:compose([By2, By3]))(1),
    ok.

twice_test() ->
    By3 = fun(X) -> X*3 end,
    TwiceBy3 = functions:twice(By3),
    18 = TwiceBy3(2),
    TwiceTwice = functions:twice(fun functions:twice/1),
    TTBy3 = TwiceTwice(By3),
    162 = TTBy3(2),
    ok.

iterate_test() ->
    By2 = fun(X) -> X*2 end,
    8 = ((functions:iterate(3))(By2))(1),
    ok.


-endif.