-module(game_test).
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

restult_test() ->
    0 = game:result(paper, paper),
    1 = game:result(paper, rock),
    1 = game:result(scissors, paper),
    -1 = game:result(rock, paper),
    -1 = game:result(paper, scissors),
    ok.

tournament_test() ->
    -1 = game:tournament([rock,rock, paper,   paper],
                         [rock,paper,scissors,rock]),
    ok.


-endif.