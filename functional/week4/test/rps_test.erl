-module(rps_test).
-ifdef(TEST).
-include_lib("eunit/include/eunit.hrl").

% Test strategies

cycle_test() ->
    First  = rps:cycle([]),
    % input is ignored, only length counts
    Second = rps:cycle([rock]),
    Third  = rps:cycle([rock, rock]),
    First  = rps:cycle([rock, rock, rock]),
    Second = rps:cycle([rock, rock, rock, rock]),
    Third  = rps:cycle([rock, rock, rock, rock, rock]),
    ok.

least_frequent_test() ->
    % here we expect `paper` -> we play `scissors`
    scissors = rps:least_frequent([rock, scissors, scissors]),
    % here we expect `scissors` -> we play `rock`
    rock = rps:least_frequent([paper, paper, scissors, rock, rock]),
    ok.

-endif.