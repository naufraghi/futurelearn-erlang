-module(pattern).
-export([max_three/3, how_many_equal/3, xor1/2, xor2/2, xor3/2, test/0]).

% Exported functions

max_three(X,Y,Z) ->
	XY = max(X, Y),
	max(XY, Z).


how_many_equal(A,A,A) -> 3;
how_many_equal(A,A,_) -> 2;
how_many_equal(A,_,A) -> 2;
how_many_equal(_,A,A) -> 2;
how_many_equal(_,_,_) -> 0.


xor1(A, B) ->
	A =/= B.

xor2(A, B) ->
	(A or B) and not(A and B).

xor3(true, B) ->
	not(B);
xor3(false, B) ->
	B.

% Tests

test_max_three() ->
	3 = max_three(1,2,3),
	3 = max_three(3,2,1),
	3 = max_three(3,1,2),
	3 = max_three(3,1,3),
	3 = max_three(2,3,3),
	ok.


test_how_many_equal() ->
	0 = how_many_equal(34,25,36),
	2 = how_many_equal(1,2,2),
	2 = how_many_equal(1,1,2),
	2 = how_many_equal(1,2,1),
	3 = how_many_equal(1,1,1),
	ok.


test_xor(Xor) ->
	false = Xor(true, true),
	false = Xor(false, false),
	true = Xor(true, false),
	true = Xor(false, true),
	ok.

% Test all

test() ->
	test_max_three(),
	test_how_many_equal(),
	test_xor(fun (A,B) -> xor1(A, B) end),
	test_xor(fun (A,B) -> xor2(A, B) end),
	test_xor(fun (A,B) -> xor3(A, B) end).
