-module(second).
-export([hypotenuse/2,area/2,perimeter/2]).

hypotenuse(X,Y) ->
	X2 = first:square(X),
	Y2 = first:square(Y),
	math:sqrt(X2 + Y2).

area(X,Y) ->
	H = hypotenuse(X,Y),
	first:area(X,Y,H).

perimeter(X,Y) ->
	H = hypotenuse(X,Y),
	X+Y+H.
