-module(sorting).
-include_lib("eunit/include/eunit.hrl").
-export([mergesort/1, merge/2, insert/2, quicksort/1, partition/2, insertionsort/1]).


% Sorting lists (2.27)

mergesort([_,_|_]=L) ->
    {L1, L2} = lists:split(length(L) div 2, L),
    S1 = mergesort(L1),
    S2 = mergesort(L2),
    merge(S1, S2);
mergesort([X]) ->
    [X].

% Sub-lists are sorted
merge([X|Xs], S2) ->
    merge(Xs, insert(X, S2));
merge([], S2) ->
    S2.

% Insert in increasing order
insert(N, L) ->
    insert(N, L, []).

% Insert the number in order, small .. big
insert(N, [X|Xs], R) when N < X ->
    % Insert N and put back what we moved out
    more2:joinr([N,X|Xs], R);
insert(N, [X|Xs], R) ->
    % if N is still bigger that X, move out X
    insert(N, Xs, [X|R]);
insert(N, [], R) ->
    % R is sorted in reverse, big .. small, and R was
    % bigger than the last item we moved out, so we can
    % prepend the N and joinr R (reversing everything).
    more2:joinr([N],R).

% Quicksort

quicksort([]) ->
    [];
quicksort([X]) ->
    [X];
quicksort([X|Xs]) ->
    {L1, L2} = partition(X, Xs),
    quicksort(L1) ++ [X|quicksort(L2)].

% Helper function to partition the elements

partition(X, Xs) ->
    partition(X, Xs, [], []).

partition(_X, [], Small, Big) ->
    {Small, Big};
partition(X, [L|Ls], Small, Big) when L > X ->
    partition(X, Ls, Small, [L|Big]);
partition(X, [L|Ls], Small, Big) ->
    partition(X, Ls, [L|Small], Big).


% Insertion sort

insertionsort([]) ->
    [];
insertionsort([X|Xs]) ->
    insertionsort(X, Xs, []).

insertionsort(X, [], L) ->
    insert(X, L);
insertionsort(X, [R|Rs], L) ->
    insertionsort(R, Rs, insert(X, L)).


% Tests

insert_test() ->
    [1,2,2,3] = insert(2, [1,2,3]),
    [1,2,3,4] = insert(4, [1,2,3]),
    [1,2] = insert(1,[2]),
    [1,2] = insert(2,[1]),
    [1] = insert(1,[]),
    ok.

merge_test() ->
    [1,4] = merge([1],[4]),
    [2,3] = merge([3],[2]),
    [1,2,3,4] = merge([3,2],[1,4]),
    ok.

partition_test() ->
    {[1],[3]} = partition(2, [1,3]),
    {[3,1,2],[]} = partition(3, [2,1,3]),
    ok.

test_sort(SortFun) ->
    [1,1,2,3,5,6,12,35] = SortFun([1,2,35,6,1,12,3,5]),
    [-11,1,2,3,5,6,12,35] = SortFun([1,2,35,6,-11,12,3,5]),
    ok.

mergesort_test() ->
    test_sort(fun mergesort/1),
    ok.

quicksort_test() ->
    test_sort(fun quicksort/1),
    ok.

insertionsort_test() ->
    test_sort(fun insertionsort/1),
    ok.
