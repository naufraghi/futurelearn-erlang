-module(recursion).
-export([fib/1, test/0, pieces/1]).

fib(0) ->
	0;
fib(1) ->
	1;
fib(N) when N > 0 ->
       fib(N - 1) + fib(N - 2).

% Expansion of the recursion
% --------------------------
%
%               fib(4) => is not fib(0) nor fib(1) so
%      fib(3)     +    fib(2) =>  are not fib(0) nor fib(1) so
% fib(2) + fib(1) + fib(1) + fib(0) => we can substitute plain values
% fib(2) +   1    +   1    +   0    => and expand
% fib(1) + fib(0) + 2 =>
% 1 + 0 + 2 = 3


pieces(0) ->
	1;
pieces(N) when N > 0 ->
	% Pieces after N straight cuts on a piece of paper
	%
	% no cuts --8<-- 1 piece   (0 lines):  pieces(0) -> 1.
	% no cuts --8<-- 2 pieces  (1 line):   pieces(1) -> 2.
	% 1 line  --8<-- 4 pieces  (2 lines):  pieces(2) -> 4.
	% 2 lines --8<-- 7 pieces  (3 lines):  pieces(3) -> 7.
	% 3 lines --8<-- 11 pieces (4 linee):  pieces(4) -> 11.
	%
	% Each new cut can meet with all previous cuts, and so
	% cut N can meet (N-1) lines, cutting N pieces.
	%
	CuttedPieces = N*2,  % where 2 is the dimension
	RemainingPieces = pieces(N - 1) - N,
	NewPieces = CuttedPieces + RemainingPieces,
	NewPieces.


% Tests


test_fib() ->
	3 = fib(4),
	5 = fib(5),
	13 = fib(7),
	144 = fib(12),
	377 = fib(14),
	ok.


test_pieces() ->
	% http://www.jlmartin.faculty.ku.edu/MiniCollege2012/handout.pdf
	1 = pieces(0),
	2 = pieces(1),
	4 = pieces(2),
	7 = pieces(3),
	11 = pieces(4),
	16 = pieces(5),
	211 = pieces(20),
	ok.


test() ->
	test_fib(),
	test_pieces().
