-module(shapes).
-include_lib("eunit/include/eunit.hrl").
-export([perimeter/1, area/1, enclose/1, rect_size/1, dist/2, x/1, y/1]).

% Shapes

% In the first example (week 1) we stored a triangle as a list of lengths, here
% I prefer to use a coordinate system to support more general shapes.

% Shapes are defined as a closed list of points.
% The last segment is implicit, and it is connected to the first.
perimeter([A|Shape]) ->
    % Here we add the closing segment.
    length([A|Shape] ++ [A], 0).

% FIXME: the only supported area is the triangle one.
area([P1, P2, P3]) ->
    A = dist(P1, P2),
    B = dist(P2, P3),
    C = dist(P3, P1),
	% Heron’s formula
	S = (A+B+C)/2,
	math:sqrt(S*(S-A)*(S-B)*(S-C)).


dist({X1,Y1}, {X2,Y2}) ->
    Dx = (X2 - X1)*(X2 - X1),
    Dy = (Y2 - Y1)*(Y2 - Y1),
    math:sqrt(Dx + Dy).

% Helper path length function.
% A meaningful length is not defined for less than 2 points.
length([A,B|Rest], Sum) ->
    S = Sum + dist(A,B),
    length([B|Rest], S);
% In the special exit case of one point, the length is not increased, because
% we reached the end of the path.
length([_], Sum) ->
    Sum.

% Helper functions to acces the coordinates.
x({X, _Y}) -> X.
y({_X, Y}) -> Y.

% Return the smallest, axis aligned, rectangle enclosing the shape.
enclose(Shape) ->
    MinX = lists:min(lists:map(fun x/1, Shape)),
    MinY = lists:min(lists:map(fun y/1, Shape)),
    MaxX = lists:max(lists:map(fun x/1, Shape)),
    MaxY = lists:max(lists:map(fun y/1, Shape)),
    {rect, {MinX, MinY}, {MaxX, MaxY}}.

% Helper function to convert a rect into the sides lengths.
rect_size({rect, {MinX, MinY}, {MaxX, MaxY}}) ->
    W = MaxX - MinX,
    H = MaxY - MinY,
    {size, W, H}.

% Tests

% Helper function for floating comparison.
almost_equal(A, B) ->
    abs(A - B) < 0.001.

% eunit compatible tests. The `test/0` function is added (and exported)
% automatically by `-include_lib("eunit/include/eunit.hrl").`.

dist_test() ->
    % Simple segment
    true = almost_equal(1, dist({0,0}, {1,0})),
    % Double length segment
    true = almost_equal(2, dist({0,-1}, {0,1})),
    % Square diagonal
    true = almost_equal(1.414, dist({0,0}, {1,1})),
    ok.

area_test() ->
    % Half unit square
    true = almost_equal(0.5, area([{0,0},{1,0},{0,1}])),
    ok.

perimeter_test() ->
    % Unit square (we have no area impl for generic shapes)
    Shape = [{0,0}, {1,0}, {1,1}, {0,1}],  % implicit close
    true = almost_equal(4, perimeter(Shape)),
    ok.

enclose_test() ->
    Shape = [{-1,-1}, {1,0}, {99,49}, {0,1}, {0,0}],
    {size, W, H} = rect_size(enclose(Shape)),
    true = almost_equal(100, W),
    true = almost_equal(50, H),
    ok.
