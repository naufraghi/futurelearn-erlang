-module(bits).
-export([bits/1, bits_rec/1, test/0, all_bits/1]).

% Count the number of 1 bits.
% Tail recursive implementation.

bits(N) ->
    bits(N, 0, 0).

bits(0, R, S) ->
    S + R;
bits(N, R, S) ->
    bits(N div 2, N rem 2, S + R).

all_bits([]) ->
    [];
all_bits([X|Xs]) ->
    [bits(X) | all_bits(Xs)].

% Direct recursion implementation

bits_rec(0) ->
    0;
bits_rec(N) ->
    R = N rem 2,
    R + bits_rec(N div 2).

% Tests

test() ->
    test_bits(),
    test_bits_rec().


test_bits() ->
    4 = bits(2#1111),
    5 = bits(2#11111),
    3 = bits(2#10101),
    ok.

test_bits_rec() ->
    4 = bits_rec(2#1111),
    5 = bits_rec(2#11111),
    3 = bits_rec(2#10101),
    ok.