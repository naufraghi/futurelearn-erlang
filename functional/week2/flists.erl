-module(flists).
-include_lib("eunit/include/eunit.hrl").
-export([product/1, maximum/1, product_direct/1, maximum_direct/1]).

% Product

product(L) ->
    product(L, 1).  % 1 is the neutral element of the product.

product([], P) -> P;
product([C|Rs], P) ->
    product(Rs, P*C).

% Product direct recursion
% To test the error test, here is undefined for an empty list.
product_direct([C]) -> C;
product_direct([C|Rs]) ->
    C * product_direct(Rs).

% Maximum

maxn(A, B) when A >= B ->
    A;
maxn(_A, B) ->
    B.

% Maximum is not defined for an empty list.
maximum([C|Rs]) ->
    maximum(Rs, C).

maximum([], M) ->
    M;
maximum([C|Rs], M) ->
    maximum(Rs, maxn(C, M)).

% Maximum direct recursion

maximum_direct([C]) ->
    C;
maximum_direct([C|Rs]) ->
    maxn(C, maximum(Rs)).

% Tests

product_test() ->
    1 = product([1]),
    1 = product([]),  % Starting the accumulator with 1 has this effect.
    6 = product([1,2,3]),
    % Now test the direct recursion
    1 = product_direct([1]),
    ?assertError(function_clause, product_direct([])),
    6 = product_direct([1,2,3]),
    ok.

maximum_test() ->
    3 = maximum([1,2,3]),
    1 = maximum([1]),
    9 = maximum([9,4,8]),
    % Now test the direct recursion
    3 = maximum_direct([1,2,3]),
    1 = maximum_direct([1]),
    9 = maximum_direct([9,4,8]),
    % just to play with strings as lists
    "c" = [maximum_direct("abc")],
    ok.