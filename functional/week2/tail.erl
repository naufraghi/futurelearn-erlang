-module(tail).
-export([sum/1, maximum/1, fib/1, perfect/1, test/0]).

% Sum

sum(L) ->
	sum(L, 0).

sum([], T) ->
	T;
sum([N|R], T) ->
	sum(R, T+N).

% Maximum

maximum([N|R]) ->
	maximum(R, N).

maximum([], M) ->
	M;
maximum([N|R], M) ->
	maximum(R, max(M, N)).

% Fibonacci
%
% fib(4) ->
% fib(4,0,1) -> no 0, continue recursion
%       / ∑
% fib(3,1,1) -> no 0, continue recursion
%       / ∑
% fib(2,1,2) -> no 0, continue recursion
%       / ∑
% fib(1,2,3) -> no 0, continue recursion
%       / ∑
% fib(0,3,5) -> first is zero, match first def
%       |
% fib(0,3,_) -> 3

fib(N) when N>=0 ->
	fib(N, 0, 1).

fib(0, F, _) ->
	F;
fib(N, F1, F2) ->
	fib(N-1, F2, F1+F2).

% Perfect
%
% We can try each divisors, from (N div 2) to 1,
% and accumulate if (N rem D) == 0. Not very performant.

perfect(N) when N>0 ->
	D = N div 2,  % biggest divisor
	R = N rem D,
	perfect(D, N, 0, R).

% helper function to avoid repetitions in perfect() impl
maybe_sum(0, S, D) -> S+D;
maybe_sum(_, S, _) -> S.

perfect(1, Num, Sum, _) ->
	Sum+1 == Num;
perfect(Div, Num, Sum, Rem) ->
	Dn = Div-1,  % next divisor
	Rn = Num rem Dn,  % next remainder
	Sn = maybe_sum(Rem, Sum, Div),  % sum if current remainder is 0
	perfect(Dn, Num, Sn, Rn).


% Tests

test() ->
	test_sum(),
	test_maximum(),
	test_fib(),
	test_perfect().

test_sum() ->
	Sum = sum([1,2,3,4,5]),
	Sum = lists:sum([1,2,3,4,5]),
	ok.

test_maximum() ->
	Max = maximum([1,2,8,3,5,1]),
	Max = lists:max([1,2,8,3,5,1]),
	ok.

test_fib() ->
	0 = fib(0),
	1 = fib(1),
	5 = fib(5),
	144 = fib(12),
	ok.

test_perfect() ->
	true = perfect(6),
	false = perfect(7),
	true = perfect(28),
	false = perfect(29),
	ok.
