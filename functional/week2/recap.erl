-module(recap).
-include_lib("eunit/include/eunit.hrl").
-export([nub/1, nub/2, palindrome/1, remove_but_last/2, reverse/1, take/2, normalize/1, has_value/2]).

% Remove duplicates, preserving the order
%
% https://gist.github.com/naufraghi/8837641f2b130d08ef0e5d78c83907e3#file-cases-erl
% contains an implementation that uses sort() to find consecutive duplicates.

nub(L) ->
   nub(L, first).

nub(L, first) ->
    nub(L, [], sets:new());
nub(L, last) ->
    bun(L, L).

nub([], U, _S) ->
    U;
nub([X|Xs], U, S) ->
    case sets:is_element(X, S) of
        false -> nub(Xs, U ++ [X], sets:add_element(X, S));
        true -> nub(Xs, U, S)
    end.

bun([], U) ->
    U;
bun([X|Xs], U) ->
    bun(Xs, remove_but_last(X, U)).

remove_but_last(X, U) ->
    remove_but_last(X, U, []).

remove_but_last(_X, [], F) ->
    F;
remove_but_last(X, [X|Us], F) ->
    case has_value(X, Us) of
        true -> remove_but_last(X, Us, F);  % if we have yet some, strip it
        false -> remove_but_last(X, Us, F ++ [X])
    end;
remove_but_last(X, [U|Us], F) ->
    remove_but_last(X, Us, F ++ [U]).

% has_value can be used as `sets:is_element` to avoid the usage of `sets` functions.

has_value(N, [N|_Xs]) ->
    true;
has_value(N, [_X|Xs]) ->
    has_value(N, Xs);
has_value(_N, []) ->
    false.

% Palindrome
%
% A list that remains the same if reversed.

palindrome(L) ->
    N = normalize(L),
    N == reverse(N).


reverse(L) ->
    reverse(L, []).

reverse([], R) ->
    R;
reverse([X|Xs], R) ->
    reverse(Xs, [X|R]).

% Palindrome for strings (strip non alfabetic chars)

normalize(S) ->
    normalize(S, " ,;.:?!'").

normalize(String, []) ->
    string:lowercase(String);
normalize(String, [I|Is]) ->
    normalize(string:join(string:replace(String, [I], "", all), ""), Is).

% Take

-spec take([T], integer()) -> [T].
take(L, N) ->
    take(L, N, []).

take([], _N, T) ->
    T;
take(_L, 0, T) ->
    T;
take([X|Xs], N, T) ->
    take(Xs, N-1, T ++ [X]).


% Tests

nub_first_test() ->
    [1,3,2] = nub([1,1,1,3,3,2], first),
    [1,3,2] = nub([1,1,3,1,2,2,3]),
    [] = nub([]),
    ok.

nub_last_test() ->
    [1,3,2] = nub([1,1,1,3,3,2], last),
    [1,2,3] = nub([1,1,3,1,2,2,3], last),
    [] = nub([]),
    ok.

has_value_test() ->
    true = has_value(2, [2,1,0]),
    true = has_value(1, [2,1,0]),
    true = has_value(0, [2,1,0]),
    false = has_value(3, [2,1,0]),
    ok.

remove_but_last_test() ->
    [2,3,1] = remove_but_last(1, [1,1,1,2,3,1]),
    [] = remove_but_last(1, []),
    ok.

reverse_test() ->
    [] = reverse([]),
    [3,2,1] = reverse([1,2,3]),
    [4,5,4,3,2,1] = reverse([1,2,3,4,5,4]),
    ok.

normalize_test() ->
    "madam" = normalize("Madam"),
    "foobar" = normalize("Foo! Bar?"),
    ok.

palidrome_test() ->
    true = palindrome([1,2,1]),
    true = palindrome("abba"),
    true = palindrome("Madam"),
    true = palindrome("Madam I'm Adam"),
    ok.

take_test() ->
    [] = take([1,2,3], 0),
    [1] = take([1,2,3], 1),
    [1,2] = take([1,2,3], 2),
    [1,2,3] = take([1,2,3], 3),
    [1,2,3] = take([1,2,3], 4),
    ok.
