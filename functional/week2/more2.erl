-module(more2).
-include_lib("eunit/include/eunit.hrl").
-export([join/2, reverse/1, joinr/2, concat/1, member/2]).

% Join lists

join(L,R) ->
    % We can compose the simpler joinr with two reverse to have the "normal" join
    reverse(joinr(reverse(L),R)).

% Helper functions that joins the reverse of the second list before the first.
joinr(L, []) ->
    L;
joinr(L, [X|Xs]) ->
    joinr([X|L], Xs).

reverse(L) ->
    reverse(L,[]).

reverse([],R) ->
    R;
reverse([X|Xs], R) ->
    reverse(Xs, [X|R]).

% Concatenate lists

concat([X,Y|Xs]) ->
    % join each recursion two elements
    concat([join(X,Y)|Xs]);
concat([X]) ->
    % We concatenated all the lists into one
    X;
concat([]) ->
    [].

% Testing membership

member(N, [N|_Xs]) ->
    true;
member(N, [_X|Xs]) ->
    member(N, Xs);
member(_N, []) ->
    false.


% Tests

join_test() ->
    "More Lists" = join("More", " Lists"),
    [1,2,3,4] = join([1,2], [3,4]),
    [1,2,3,4] = join([], [1,2,3,4]),
    [1,2,3,4] = join([1,2,3,4], []),
    ok.

reverse_test() ->
    [3,2,1] = reverse([1,2,3]),
    [] = reverse([]),
    ok.

concat_test() ->
    "More Lists" = concat(["More", " ", "Lists"]),
    [1,2,3,4] = concat([[1], [], [2,3,4], []]),
    [] = concat([]),
    1 = concat([1]),  % unsure about this
    % Concat is undefined a list of non lists
    ?assertError(function_clause, concat([1,2])),
    ok.
