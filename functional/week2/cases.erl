-module(cases).
-include_lib("eunit/include/eunit.hrl").
-export([median/1, nub/1, mode/1, modes/1, double/1, evens/1]).

% Remove duplicates `nub`

nub(L) ->
    nub(lists:sort(L), []).

% Operates on sorted lists.
nub([], U) ->
    U;
nub([X|Xs], [X|_]=U) ->
    nub(Xs, U);
nub([X|Xs], U) ->
    nub(Xs, [X|U]).

% Doubles

double(L) ->
    % Should have used map, but, without it:
    lists:reverse(double(L, [])).

double([], D) ->
    D;
double([X|Xs], D) ->
    % [1|2,3] -> [1]
    % [2|3] -> [4,2]
    % [3] -> [6,4,2]
    % The list order is reversed.
    double(Xs, [2*X|D]).

% Evens

evens(L) ->
    lists:reverse(evens(L, [])).

evens(L, E) ->
    case L of
        [] -> E;
        [X|Xs] when X rem 2 == 0 -> evens(Xs, [X|E]);
        [_X|Xs] -> evens(Xs, E)
    end.

% Median

median(L) ->
    S = lists:sort(L),  % [1,2,3]
    N = length(L) + 1,  % indexing starts at 1 -> 4
    M = N div 2,  % -> 2
    R = N rem 2,  % -> 0
    Median1 = lists:nth(M, S),
    Median2 = lists:nth(M+R, S),
    % Doing always the mean, we return always a float value.
    (Median1 + Median2) / 2.

% Mode

mode(L) ->
    % a side effect of this sorting is that the mode of multimodal
    % list is the biggest number.
    {_,M} = lists:max(count(lists:sort(L), [])),
    M.

modes(L) ->
    Counts = count(lists:sort(L), []),
    Sorted = lists:sort(fun (X,Y) -> X>Y end, Counts),
    [{C,_}|_] = Sorted,
    Modes = lists:filter(fun ({Cx,_}) -> Cx == C end, Sorted),
    lists:map(fun ({_,N}) -> N end, Modes).


% Operates on a sorted list.
count([], M) ->
    M;
% Here we increment the count of matching numbers
count([N|Xs], [{C,N}|Ms]) ->
    count(Xs,[{C+1,N}|Ms]);
% If there is no match, we start a new count.
count([X|Xs], M) ->
    count(Xs,[{1,X}|M]).

% Tests

nub_test() ->
    [3,2,1] = nub([1,1,2,1,3,3,2]),
    ok.

double_test() ->
    [] = double([]),
    [2,4,6] = double([1,2,3]),
    ok.

evens_test() ->
    [] = evens([]),
    [2,4] = evens([1,2,3,4]),
    ok.

median_test() ->
    2.0 = median([1,2,3]),
    2.0 = median([1,2,2,3]),
    2.5 = median([1,2,3,4]),
    ok.

mode_test() ->
    3 = mode([1,2,3]),  % see above comment
    1 = mode([1,2,3,1]),
    2 = mode([1,2,3,1,2]),  % see above comment
    1 = mode([1,2,3,1,2,1]),
    ok.

modes_test() ->
    [3,2,1] = modes([1,2,3]),  % see above comment
    [1] = modes([1,2,3,1]),
    [2,1] = modes([1,2,3,1,2]),  % see above comment
    [1] = modes([1,2,3,1,2,1]),
    ok.
