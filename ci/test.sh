#!/usr/bin/env sh

set -e

echo "********************************"
echo "* Functional Erlang Course     *"
echo "********************************"
(cd functional/week3; rebar3 eunit -v)
(cd functional/week4; rebar3 eunit -v)

echo "********************************"
echo "* Concurrent Erlang Course     *"
echo "********************************"
(cd concurrent/week2; rebar3 eunit -v)
